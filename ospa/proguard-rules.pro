# Use Proguard only for minifiaction (reduces apk size by 50%)
# Obfuscating code is not useful because Defendroid is Open Source

-dontobfuscate
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontpreverify
-optimizations !*

-keepattributes *
-keep interface ** { *; }
-keep class ** { *; }
-keep enum ** { *; }

-dontwarn **