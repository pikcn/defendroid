package com.pikcn.defendroid.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.preference.PreferenceManager;

import com.pikcn.defendroid.R;
import com.pikcn.defendroid.databinding.FragmentEulaBinding;

public class EulaFragment extends Fragment {

    private FragmentEulaBinding binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentEulaBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        binding.buttonAcceptEula.setOnClickListener(buttonView -> {
            final SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(buttonView.getContext());
            pref.edit().putBoolean("eula_accepted", true).apply();
            final NavController controller = NavHostFragment.findNavController(EulaFragment.this);
            controller.navigate(R.id.action_eula_to_scan);
            final FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.invalidateOptionsMenu();
            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        final NavController controller = NavHostFragment.findNavController(EulaFragment.this);
        if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean("eula_accepted", false)) {
            controller.navigate(R.id.action_eula_to_scan);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}