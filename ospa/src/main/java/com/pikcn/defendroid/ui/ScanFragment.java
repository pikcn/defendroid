package com.pikcn.defendroid.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.pikcn.defendroid.R;
import com.pikcn.defendroid.app.ScanWorker;
import com.pikcn.defendroid.databinding.FragmentScanBinding;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ScanFragment extends Fragment {
    private FragmentScanBinding binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentScanBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final FragmentActivity activity = getActivity();
        if (activity != null && binding != null) {
            // Initial Screen, only button is visible //
            binding.progressScan.setVisibility(View.INVISIBLE);
            binding.textScan.setVisibility(View.INVISIBLE);

            final ExecutorService executor = Executors.newSingleThreadExecutor();
            final Observer<Integer> observer = state -> {
                switch (state) {
                    case 1: // ENQUEUED
                        binding.textScan.setText(R.string.initiating_scan);
                        binding.progressScan.setIndeterminate(true);
                        binding.progressScan.setIndicatorDirection(LinearProgressIndicator.INDICATOR_DIRECTION_END_TO_START);
                        break;
                    case 2: // RUNNING
                        binding.textScan.setText(R.string.scanning);
                        binding.progressScan.setIndeterminate(true);
                        binding.progressScan.setIndicatorDirection(LinearProgressIndicator.INDICATOR_DIRECTION_START_TO_END);
                        break;
                    case 3: // SUCCEEDED
                        binding.textScan.setText(R.string.scan_complete);
                        binding.progressScan.setVisibility(View.INVISIBLE);
                        final NavController controller = NavHostFragment.findNavController(ScanFragment.this);
                        controller.navigate(R.id.action_scan_to_apps);
                        break;
                    case 4: // FAILED
                    default:
                        binding.textScan.setText(R.string.error_during_scan);
                        binding.progressScan.setIndeterminate(false);
                        break;
                }
            };

            binding.btnScan.setOnClickListener(buttonView -> {
                executor.execute(new ScanWorker(activity.getApplicationContext(), observer));
                binding.progressScan.setVisibility(View.VISIBLE);
                binding.textScan.setVisibility(View.VISIBLE);
            });
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}