package com.pikcn.defendroid.ui.apps;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pikcn.defendroid.app.AppsScanResult;
import com.pikcn.defendroid.databinding.FragmentAppsAdvisoryBinding;

import java.util.List;

public class AppsAdvisoryRecyclerViewAdapter extends RecyclerView.Adapter<AppsAdvisoryViewHolder> {
    private final List<AppsScanResult> mValues;
    public AppsAdvisoryRecyclerViewAdapter(List<AppsScanResult> items) {
        mValues = items;
    }

    @NonNull
    @Override
    public AppsAdvisoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        @NonNull final FragmentAppsAdvisoryBinding view = FragmentAppsAdvisoryBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new AppsAdvisoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AppsAdvisoryViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mTitle.setText(holder.mItem.getDisplayName());
        holder.mIcon.setImageDrawable(holder.mItem.getIcon());
        holder.mDescription.setText(holder.mItem.getDescription());
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }
}