package com.pikcn.defendroid.app;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ScanWorker implements Runnable {
    private static final Set<AppsScanResult> APPS_SCAN_RESULTS = new HashSet<>();
    private static final Set<SettingsScanResult> SETTINGS_SCAN_RESULTS = new HashSet<>();
    private Context context;
    private final Observer<Integer> observer;
    private static final Handler HANDLER = new Handler(Looper.getMainLooper());

    public ScanWorker(@NonNull final Context context, @NonNull final Observer<Integer> observer) {
        this.context = context.getApplicationContext();
        this.observer = observer;
        HANDLER.post(() -> observer.onChanged(1));    // ENQUEUED
    }

    @Override
    public void run() {
        try {
            HANDLER.post(() -> observer.onChanged(2));  // RUNNING
            scanApps();
            scanSettings();
            HANDLER.post(() -> observer.onChanged(3));  // SUCCEEDED
        } catch (Exception e) {
            HANDLER.post(() -> observer.onChanged(4));  // FAILED
            Log.d(ScanWorker.class.getCanonicalName(), "Unexpected error in ScanWorker thread", e);
        } finally {
            context = null;
        }
    }

    private synchronized void scanApps() {
        synchronized (APPS_SCAN_RESULTS) {
            APPS_SCAN_RESULTS.clear();
            APPS_SCAN_RESULTS.addAll(AppsScanner.appAnalysis(context.getApplicationContext()));
        }
    }

    private synchronized void scanSettings() {
        synchronized (SETTINGS_SCAN_RESULTS) {
            SETTINGS_SCAN_RESULTS.clear();
            SETTINGS_SCAN_RESULTS.addAll(SettingsScanner.systemAnalysis(context.getApplicationContext()));
        }
    }

    public static List<AppsScanResult> getAppsScanResults() {
        synchronized (APPS_SCAN_RESULTS) {
            return new ArrayList<>(APPS_SCAN_RESULTS);
        }
    }

    public static List<SettingsScanResult> getSettingsScanResults() {
        synchronized (SETTINGS_SCAN_RESULTS) {
            return new ArrayList<>(SETTINGS_SCAN_RESULTS);
        }
    }
}