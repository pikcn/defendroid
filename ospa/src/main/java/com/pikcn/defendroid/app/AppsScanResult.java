package com.pikcn.defendroid.app;

import android.graphics.drawable.Drawable;
import android.view.View;

public class AppsScanResult implements View.OnClickListener {
    private final Drawable icon;
    private final String displayName;
    private final String description;


    public AppsScanResult(String appName, String packageName, String description, Drawable icon) {
        this.displayName = appName + " (" + packageName + ")";
        this.description = description;
        this.icon = icon;
    }

    @Override
    public void onClick(final View view) {

    }

    public Drawable getIcon() {
        return icon;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getDescription() {
        return description;
    }
}
