package com.pikcn.defendroid.app;

import android.os.Parcelable;
import android.view.View;

public class SettingsScanResult implements View.OnClickListener {
    public static final String SEVERITY_CRITICAL = "CRITICAL";
    public static final String SEVERITY_HIGH = "HIGH";
    public static final String SEVERITY_MEDIUM = "MEDIUM";
    public static final String SEVERITY_LOW = "LOW";

    private final String title;
    private final String description;
    private final String severity;
    private Parcelable launchIntent;

    public SettingsScanResult(final String title, final String description, final String severity) {
        this.title = title;
        this.description = description;
        this.severity = severity;
    }


    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getSeverity() {
        return severity;
    }

    @Override
    public void onClick(View view) {

    }
}
