package com.pikcn.defendroid.app;

import android.graphics.drawable.Drawable;
import android.os.Build;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class AppDetail {
    public String appName;
    public String packageName;
    public boolean isSystemApp;
    public int targetSdkVersion;
    public int compileSdkVersion;
    public boolean internetPermission;

    public Set<String> installer = new HashSet<>();
    public boolean usesClearTextTraffic;
    public boolean usesAccessibilityService;
    public boolean usesDeviceAdmin;
    private boolean isOld;
    private boolean unverifiedSource;
    public boolean isUnlaunchable;
    public boolean hasInstallPermission;


    public void compute() {
        isOld = (targetSdkVersion > 0 && Build.VERSION.SDK_INT - targetSdkVersion > 2) || (compileSdkVersion > 0 && Build.VERSION.SDK_INT - compileSdkVersion > 2);
        unverifiedSource = !isVerifiedSource();
    }

    private boolean isVerifiedSource() {
        for (String source : installer) {
            if (!source.equals("com.android.vending") && !source.endsWith("(com.android.vending)")) {
                return false;
            }
        }
        return true;
    }


    public boolean isUnsafe() {
        return isOld || unverifiedSource || usesClearTextTraffic || usesAccessibilityService || usesDeviceAdmin || isUnlaunchable || hasInstallPermission;
    }

    @NonNull
    @Override
    public String toString() {
        List<String> messages = getMessages();
        if (messages.isEmpty()) {
            return "This app is safe";
        } else {
            return String.join(System.lineSeparator(), messages);
        }
    }

    private List<String> getMessages() {
        List<String> messages = new ArrayList<>();
        if (isOld) {
            if (compileSdkVersion == targetSdkVersion || compileSdkVersion == 0) {
                messages.add(String.format(Locale.US, "App made for %s, your system is Android %s.",
                        DroidHelper.getOsName(targetSdkVersion), DroidHelper.getOsName(Build.VERSION.SDK_INT)));
            } else if (compileSdkVersion < targetSdkVersion) {
                messages.add(String.format(Locale.US, "App compiled for %s, your system is Android %s.",
                        DroidHelper.getOsName(compileSdkVersion), DroidHelper.getOsName(Build.VERSION.SDK_INT)));
            } else {
                messages.add(String.format(Locale.US, "App compiled for %s, your system is Android %s.",
                        DroidHelper.getOsName(targetSdkVersion), DroidHelper.getOsName(Build.VERSION.SDK_INT)));
            }
        }
        if (unverifiedSource) {
            // App not from Play Store, but installed via adb.
            final String installerName = installer.size() == 1 ? installer.toArray(new String[1])[0] : installer.toString();
            messages.add(String.format(Locale.US, "App not from Play Store, installed via %s.", installerName));
        }

        if (usesClearTextTraffic) {
            messages.add("App may use insecure networking.");
        }

        if (usesAccessibilityService) {
            messages.add("App may misuse Accessibility Services.");
        }

        if (usesDeviceAdmin) {
            messages.add("App uses deprecated, insecure device admin features.");
        }

        if (isUnlaunchable) {
            messages.add("Hidden App.");
        }

        if (hasInstallPermission) {
            messages.add("App can install other apps.");
        }

        return messages;
    }

    public AppsScanResult toModel(Drawable icon) {
        return new AppsScanResult(appName, packageName, this.toString(), icon);
    }
}
