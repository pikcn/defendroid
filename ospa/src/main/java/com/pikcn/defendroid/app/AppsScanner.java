package com.pikcn.defendroid.app;

import android.Manifest;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.InstallSourceInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.os.Build;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

public final class AppsScanner {
    private AppsScanner()  { throw new UnsupportedOperationException(); }
    public static List<AppsScanResult> appAnalysis(final Context context) {
        return getAppList(context.getPackageManager());
    }

    private static List<AppsScanResult> getAppList(PackageManager pm) {
        List<PackageInfo> packList = getInstalledPackages(pm);
        List<AppsScanResult> apps = new ArrayList<>();

        for (int i = 0; i < packList.size(); i++) {
            PackageInfo packInfo = packList.get(i);
            if (!isSystemPackage(packInfo.applicationInfo)) {
                AppDetail app = getAppDetail(pm, packInfo);
                app.compute();
                if (app.isUnsafe()) {
                    apps.add(app.toModel(packInfo.applicationInfo.loadIcon(pm)));
                }
            }
        }
        return apps;
    }

    private static List<PackageInfo> getInstalledPackages(final PackageManager pm) {
        if (Build.VERSION.SDK_INT >= 34) {
            return pm.getInstalledPackages(PackageManager.PackageInfoFlags.of(DroidHelper.FLAGS | PackageManager.GET_ATTRIBUTIONS_LONG));
        } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.TIRAMISU) {
            return pm.getInstalledPackages(PackageManager.PackageInfoFlags.of(DroidHelper.FLAGS));
        } else {
            return pm.getInstalledPackages(DroidHelper.FLAGS);
        }
    }

    private static AppDetail getAppDetail(PackageManager pm, PackageInfo packInfo) {
        AppDetail app = new AppDetail();
        populateAppInfo(pm, app, packInfo);
        populateInstallSourceInfo(pm, app);
        populateDeviceAdminInfo(app, packInfo);
        populateAccessibilityInfo(app, packInfo);
        populatePermissionInfo(app, packInfo);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            packInfo.applicationInfo.getRequestRawExternalStorageAccess();
        }
        return app;
    }

    private static void populatePermissionInfo(AppDetail app, PackageInfo packInfo) {
        if (packInfo.requestedPermissions != null && packInfo.requestedPermissions.length > 0) {
            final HashSet<String> permissions = new HashSet<>(Arrays.asList(packInfo.requestedPermissions));
            app.internetPermission = permissions.contains(Manifest.permission.INTERNET);
            app.hasInstallPermission = permissions.contains(Manifest.permission.INSTALL_PACKAGES) ||
                    permissions.contains(Manifest.permission.REQUEST_INSTALL_PACKAGES);
        }
        app.usesClearTextTraffic = (packInfo.applicationInfo.flags & ApplicationInfo.FLAG_USES_CLEARTEXT_TRAFFIC) > 0;
    }

    private static void populateDeviceAdminInfo(final AppDetail app, final PackageInfo packInfo) {
        if (packInfo.receivers != null) {
            for (ActivityInfo info : packInfo.receivers) {
                if ("android.permission.BIND_DEVICE_ADMIN".equals(info.permission) || (info.metaData != null && info.metaData.getInt("android.app.device_admin", 0) != 0)) {
                    app.usesDeviceAdmin = true;
                }
            }
        }
    }
    private static void populateAccessibilityInfo(final AppDetail app, final PackageInfo packInfo) {
        if (packInfo.services != null) {
            for (ServiceInfo info : packInfo.services) {
                if ("android.permission.BIND_ACCESSIBILITY_SERVICE".equals(info.permission) || (info.metaData != null && info.metaData.getInt("android.accessibilityservice", 0) != 0)) {
                    app.usesAccessibilityService = true;
                }
            }
        }
    }

    private static void populateInstallSourceInfo(PackageManager pm, AppDetail app) {
        try {
            final InstallSourceInfo installer = pm.getInstallSourceInfo(app.packageName);
            final HashSet<String> installers = new HashSet<>();
            if (installer.getInitiatingPackageName() != null) {
                installers.add(installer.getInitiatingPackageName());
            }
            if (installer.getInstallingPackageName() != null) {
                installers.add(installer.getInstallingPackageName());
            }
            if (installer.getOriginatingPackageName() != null) {
                installers.add(installer.getOriginatingPackageName());
            }
            if (Build.VERSION.SDK_INT >= 34 && installer.getUpdateOwnerPackageName() != null) {
                installers.add(installer.getUpdateOwnerPackageName());
            }
            if (installers.isEmpty() && !app.isSystemApp) {
                app.installer.add("adb");
            } else {
                for (String pkg : installers) {
                    final String name = getAppNameFromPkgName(pm, pkg);
                    // Changing this format will effect AppDetail::isVerifiedSource
                    app.installer.add(name == null ? pkg : String.format(Locale.US, "%s (%s)", name, pkg));
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d(AppsScanner.class.getCanonicalName(), "Cannot resolve " + app.packageName, e);
        }
    }

    private static void populateAppInfo(PackageManager pm, AppDetail app, PackageInfo packInfo) {
        app.packageName = packInfo.applicationInfo.packageName;
        app.appName = String.valueOf(packInfo.applicationInfo.loadLabel(pm));
        app.targetSdkVersion = packInfo.applicationInfo.targetSdkVersion;
        app.compileSdkVersion = Build.VERSION.SDK_INT >= Build.VERSION_CODES.S ? packInfo.applicationInfo.compileSdkVersion : 0;
        app.isSystemApp = isSystemPackage(packInfo.applicationInfo);
        app.isUnlaunchable = pm.getLaunchIntentForPackage(app.packageName) == null;
    }

    public static String getAppNameFromPkgName(PackageManager packageManager, String packageName) {
        try {
            ApplicationInfo info = packageManager.getApplicationInfo(packageName, PackageManager.GET_META_DATA);
            String appName = (String) packageManager.getApplicationLabel(info);
            if (appName.length() > 0) {
                return appName;
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d(AppsScanner.class.getCanonicalName(), "Cannot resolve " + packageName, e);
        }
        return null;
    }

    private static boolean isSystemPackage(ApplicationInfo pkgInfo) {
        return ((pkgInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0);
    }
}
