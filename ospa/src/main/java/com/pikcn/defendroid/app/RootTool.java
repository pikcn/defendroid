package com.pikcn.defendroid.app;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public final class RootTool {
    private static final String GOLDFISH = "goldfish";
    private static final String RANCHU = "ranchu";
    private static final String SDK = "sdk";
    private static final String GOOGLE_SDK = "google_sdk";
    private static final String[] FILES_TO_DETECT = {
            "/sbin/su",
            "/system/bin/su",
            "/system/xbin/su",
            "/data/local/xbin/su",
            "/data/local/bin/su",
            "/system/sd/xbin/su",
            "/system/bin/failsafe/su",
            "/data/local/su",
            "/su/bin/su",
            "/system/bin/.ext/su",
            "/system/usr/we-need-root/su",
            "/cache/su", "/data/su", "/dev/su"
    };

    private static final String[] ROOT_APPS = {
            "com.topjohnwu.magisk",
            "eu.chainfire.supersu",
            "com.koushikdutta.superuser",
            "com.noshufou.android.su",
            "me.phh.superuser"
    };

    private RootTool() { throw new UnsupportedOperationException(); }

    public static boolean isRooted(PackageManager packageManager) {
        // No reliable way to determine if an android phone is rooted, since a rooted phone could
        // always disguise itself as non-rooted. Some common approaches can be found on SO:
        // https://stackoverflow.com/questions/1101380/determine-if-running-on-a-rooted-device
        if (isEmulator()) {
            return true;
        }
        if (Build.TAGS != null && Build.TAGS.contains("test-keys")) {
            // check user debug modes also.TEST KEYS
            // DEV KEYS, NON RELEASE KEYS, DANGEROUS PROPS
            return true;
        }

        for (String filePath : FILES_TO_DETECT) {
            File file = new File(filePath);
            if (file.exists()) {
                return true;
            }
        }

        final String paths = System.getenv("PATH");
        if (paths != null) {
            for (String pathDir : paths.split(":")) {
                final File file = new File(pathDir, "su");
                if (file.exists()) {
                    return true;
                }
            }
        }

        final HashSet<String> installedApps = systemPackages(packageManager);
        installedApps.retainAll(Arrays.asList(ROOT_APPS));
        if (!installedApps.isEmpty()) {
            return true;
        }

        return isSuBinPresent();
    }

    private static HashSet<String> systemPackages(final PackageManager packageManager) {
        final HashSet<String> resultSet = new HashSet<>();
        final List<PackageInfo> packList = packageManager.getInstalledPackages(0);
        for (final PackageInfo packInfo: packList) {
            if ((packInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) {
                resultSet.add(packInfo.packageName);
            }
        }
        return resultSet;
    }

    public static boolean isEmulator() {
        return Build.PRODUCT.contains(SDK) || Build.PRODUCT.contains(GOOGLE_SDK) || Build.HARDWARE.contains(GOLDFISH) || Build.HARDWARE.contains(RANCHU);
    }

    public static boolean isSuBinPresent() {
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(new String[]{"/system/bin/which", "su"});

            try (final BufferedInputStream in = new BufferedInputStream(process.getInputStream());
                 final BufferedInputStream err = new BufferedInputStream(process.getErrorStream())) {
                int processResult = process.waitFor();
                final String stdout = readAllText(in, StandardCharsets.UTF_8);
                final String stderr = readAllText(err, StandardCharsets.UTF_8);
                Log.d(RootTool.class.getCanonicalName(), "process result: " + processResult);
                Log.d(RootTool.class.getCanonicalName(), "stdout: " + stdout);
                Log.d(RootTool.class.getCanonicalName(), "stderr: " + stderr);

                return stdout != null || (stderr != null && stderr.contains("permission denied"));
            }
        }
        catch (InterruptedException ie) {
            Log.d(RootTool.class.getCanonicalName(), "Runtime::exec was interrupted for 'which su'", ie);
            Thread.currentThread().interrupt();
            return false;
        }
        catch (Exception ex) {
            Log.d(RootTool.class.getCanonicalName(), "Error in Runtime::exec of 'which su'", ex);
            return false;
        } finally {
            if (process != null) process.destroy();
        }
    }

    public static String readAllText(final InputStream inputStream, final Charset charset) {
        byte[] data = readAllBytes(inputStream);
        if (data.length == 0) {
            return null;
        } else {
            return new String(data, charset);
        }
    }

    @NonNull
    private static byte[] readAllBytes(final InputStream inputStream) {
        byte[] buffer = new byte[8 * 1024];
        try (final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
            int temp;
            while ((temp = inputStream.read(buffer)) != -1) {
                byteArrayOutputStream.write(buffer, 0, temp);
            }
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            Log.d(RootTool.class.getCanonicalName(), "Cannot read stream", e);
            return new byte[0];
        }
    }
}