# Privacy Policy for Defendroid
**Effective Date:** 5th October, 2023

## 1. Introduction
This Privacy Policy explains how Defendroid or Pikcn Technologies (“we,” “us,” or “our”) collects, uses, and protects your data when you use the Defendroid application (“Application”). The Application is distributed under the GNU Affero General Public License version 3.0 (AGPLv3) and is designed to provide security and privacy advisory services for non-commercial purposes only. Please read this Privacy Policy carefully before using the Application.

## 2. Data Collection and Usage
Defendroid does not collect, transmit, or store any personally identifiable information (PII). The Application operates entirely offline, and no data is shared with external services or servers. It does not have internet or network permissions, ensuring that all data, including collated information, is processed exclusively on your device.

## 3. Information Processed by Defendroid
Defendroid analyzes the following information on your device:

**Installed Applications:** The Application scans the list of installed applications on your device to identify potential security vulnerabilities and privacy issues in those applications.

**System Settings:** Defendroid checks your device’s system settings to suggest improvements that enhance security and privacy configurations.

**Open Source Libraries:** The Application may use various open-source libraries to provide its functionality and adheres to their license terms.

## 4. Purpose Limitation
Defendroid is an open-source security and privacy advisory tool designed exclusively for non-commercial purposes. It is not an antivirus application.

## 5. Limitation of Liability
While Defendroid aims to enhance your device’s security and privacy, we are not responsible for any damage or loss that may occur, including those not highlighted by the app. The Application is provided “as is” without warranties.

## 6. Distribution
Defendroid is distributed solely via the Google Play Store under the terms of AGPLv3.

## 7. Contact Information
If you have any questions, concerns, or requests related to this Privacy Policy or the Application, please contact us at info@pikcn.com.

## 8. Changes to Privacy Policy
This Privacy Policy may be updated from time to time. Any changes will be reflected with an updated “Effective Date” at the beginning of the policy. It is your responsibility to review this Privacy Policy periodically to stay informed about how we protect your information.

>By using Defendroid, you acknowledge that you have read, understood, and agree to be bound by this Privacy Policy.